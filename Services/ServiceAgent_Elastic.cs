﻿using ClassTreeController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassTreeController.Services
{
    public class ServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private object serviceLocker = new object();

        
        public List<clsOntologyItem> ClassList
        {
            get
            {
                return ResultData?.ClassList;
            }
        }

        private ResultClasses resultData;
        public ResultClasses ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private ResultPath resultPathItems;
        public ResultPath ResultPathItems
        {
            get { return resultPathItems; }
            set
            {
                resultPathItems = value;
                RaisePropertyChanged(nameof(ResultPathItems));
            }
        }


        public async Task<ResultClasses> SearchClasses(string className)
        {
            var result = new ResultClasses
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            lock(serviceLocker)
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                var searchClass = new List<clsOntologyItem> { new clsOntologyItem { Name = className } };

                result.Result = dbReader.GetDataClasses(searchClass);

                if (result.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    result.ClassList = dbReader.Classes1.Where(classItm => classItm.Name.ToLower().Contains(className.ToLower())).ToList();

                }

            }
            

            ResultData = result;
            return result;
        }

        private List<clsOntologyItem> GetDataLevel(int subLevels, string idParent = null)
        {
            var classList = new List<clsOntologyItem>();
            var result = localConfig.Globals.LState_Success.Clone();

            try
            {
                lock (serviceLocker)
                {
                    var dbReaderClasses = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
                    var searchClasses = new List<clsOntologyItem>();


                    do
                    {
                        if (!classList.Any())
                        {
                            searchClasses.Add(new clsOntologyItem
                            {
                                GUID_Parent = idParent == null ? localConfig.Globals.Root.GUID : idParent
                            });
                        }
                        else
                        {
                            searchClasses = dbReaderClasses.Classes1.Select(cls => new clsOntologyItem { GUID_Parent = cls.GUID }).ToList();
                        }
                        result = dbReaderClasses.GetDataClasses(searchClasses);
                        
                        classList.AddRange(dbReaderClasses.Classes1.Select(cls => cls.Clone()));
                        subLevels--;
                    } while (subLevels >= 0);
                }

                

                return classList;
            }
            catch (Exception)
            {

                return null;
            }

            

        }

        public async Task<ResultPath> GetPathItems(string idClass = null)
        {
            var result = new ResultPath
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            result.PathItems = new List<clsOntologyItem>
            {
                localConfig.Globals.Root
            };

            if (idClass == localConfig.Globals.Root.GUID)
            {
                result.PathItems = new List<clsOntologyItem> { localConfig.Globals.Root };
                ResultPathItems = result;
                return result;
            }

            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            if (string.IsNullOrEmpty(idClass))
            {
                ResultPathItems = result;
                return result;
            }

            result.Result = dbReader.GetDataClasses(new List<clsOntologyItem> { new clsOntologyItem { GUID = idClass } });

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            var classItem = dbReader.Classes1.First();
            result.PathItems.Add(classItem);

            while (classItem.GUID_Parent != localConfig.Globals.Root.GUID)
            {
                result.Result = dbReader.GetDataClasses(new List<clsOntologyItem> { new clsOntologyItem { GUID = classItem.GUID_Parent } });
                if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    break;
                }
                classItem = dbReader.Classes1.First();
                result.PathItems.Insert(1, classItem);
            }

            ResultPathItems = result;
            return result;

        }

        public async Task<ResultClasses> GetDataTwoLevels(string idParent = null)
        {
            var result = new ResultClasses
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var resultItems = GetDataLevel(2, idParent);

            if (resultItems == null)
            {
                result.Result = localConfig.Globals.LState_Error.Clone();
                return result;
            }

            result.ClassList = resultItems;
            ResultData = result;


            return result;
        }

        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            
        }
    }

    public class ResultClasses
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> ClassList { get; set; }
    }

    public class ResultPath
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> PathItems { get; set; }
    }

}
