﻿using ClassTreeController.BusinessFactory;
using ClassTreeController.Models;
using ClassTreeController.Services;
using ClassTreeController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Services;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using ClassTreeController.StateMachines;

namespace ClassTreeController.Controllers
{
    
    public class ClassTreeViewController : ClassTreeViewModel, IViewController
    {
        
        private WebsocketServiceAgent webSocketServiceAgent;

        private ServiceAgent_Elastic serviceAgentElastic;
        private ViewTreeFactory viewTreeFactory;

        private TranslationController translationController = new TranslationController();

        public IControllerStateMachine StateMachine { get; private set; }
        public ClassTreeStatemachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ClassTreeStatemachine)StateMachine;
            }
        }

        private clsLocalConfig localConfig;

        public ClassTreeViewController()
        {
            
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += ClassTreeViewController_PropertyChanged;
        }

        private void ClassTreeViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.Navigation_Url_TreeData)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
                var selectNodeRequest = new InterServiceMessage
                {
                    ChannelId = Channels.SelectedClassNode,
                    SenderId = webSocketServiceAgent.EndpointId,
                    OItems = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = Text_SelectedNodeId
                        }
                    }
                };

                webSocketServiceAgent.SendInterModMessage(selectNodeRequest);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Navigation_PathNodes)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_Search)
            {
                localConfig.SearchNode.Name = "Search: " + Text_Search;
                var classListTask = serviceAgentElastic.SearchClasses(Text_Search);

                //SendTreeLevel(classList, localConfig.SearchNode.Id);
                //PathNodes = new List<ViewTreeNode> { localConfig.SearchNode };
            }
        }

        private void Initialize()
        {
            viewTreeFactory = new ViewTreeFactory(localConfig);
            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            localConfig.SearchNode = viewTreeFactory.CrateSearchNode();

            var stateMachine = new ClassTreeStatemachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            LocalStateMachine.PropertyChanged += LocalStateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            serviceAgentElastic = null;
            translationController = null;
            viewTreeFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            Text_View = translationController.Text_View;
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AddClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ChangeClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.MarkClassNodes,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.RemoveClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            LocalStateMachine.Initialize();
            serviceAgentElastic.GetPathItems();
            var classListTask = serviceAgentElastic.GetDataTwoLevels();
            classListTask.Wait();

            if (classListTask.Result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                LocalStateMachine.ClassLoaded();
                IsSuccessful_Login = false;
                return;
            }
            
            //SendTreeLevel(classListTask.Result.ClassList);

        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            Text_SelectedNodeId = oItemSelected.GUID;

            if (serviceAgentElastic.ClassList.Any(clsItem => clsItem.GUID == Text_SelectedNodeId))
            {
                SendTreeLevel(serviceAgentElastic.ClassList, Text_SelectedNodeId);
                PathNodes = viewTreeFactory.CreatePathNodes(serviceAgentElastic.ClassList, Text_SelectedNodeId);
            }
        }

        private void LocalStateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
            else if (e.PropertyName == nameof(ClassTreeStatemachine.ViewState))
            {
                if (LocalStateMachine.ShowLoader)
                {
                    webSocketServiceAgent.SendCommand(Commands.OpenLoader);
                }
                else
                {
                    webSocketServiceAgent.SendCommand(Commands.CloseLoader);

                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            
        }

      

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
                {
                    if (Text_SelectedNodeId == webSocketServiceAgent.ChangedProperty.Value.ToString()) return;
                    if (!localConfig.Globals.is_GUID(webSocketServiceAgent.ChangedProperty.Value.ToString())) return;
                    Text_SelectedNodeId = webSocketServiceAgent.ChangedProperty.Value.ToString();

                    LocalStateMachine.ClassSelectRequest();
                    serviceAgentElastic.GetPathItems(Text_SelectedNodeId);
                    serviceAgentElastic.GetDataTwoLevels(Text_SelectedNodeId);
                    
                    
                    
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.Navigation_Text_Search)
                {
                    LocalStateMachine.ClassSearchRequest();
                    Text_Search = webSocketServiceAgent.ChangedProperty.Value.ToString();
                   
                }
            }


        }


        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectClassNode)
            {
                if (message.OItems.Any())
                {
                    var oItem = message.OItems.First();
                    LocalStateMachine.SetItemSelected(oItem);

                    
                }
            }
        }

        private void SendTreeLevel(List<clsOntologyItem> classList, string idParent = null)
        {
            if (classList == null) return;

            var result = viewTreeFactory.CreateTreeNodes(classList, 1,idParent);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return;
            }

            var treeFileName = Guid.NewGuid().ToString() + ".json";
            var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
            if (sessionFile.FileUri == null)
            {
                return;
            }

            if (sessionFile.StreamWriter == null)
            {

                return;
            }

            using (sessionFile.StreamWriter)
            {
                result = viewTreeFactory.WriteJsonDoc(sessionFile.StreamWriter);

            }
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
             
                return;
            }

            Url_TreeData = sessionFile.FileUri.AbsoluteUri;

        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData && serviceAgentElastic != null)
            {

                
                if (serviceAgentElastic.ResultData.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }

                viewTreeFactory = new ViewTreeFactory(localConfig);

                var result = viewTreeFactory.CreateTreeNodes(serviceAgentElastic.ClassList,-1,null);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;
                }

                var treeFileName = Guid.NewGuid().ToString() + ".json";

                var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
                if (sessionFile.FileUri == null)
                {
                    return;
                }

                if (sessionFile.StreamWriter == null)
                {

                    return;
                }

                using (sessionFile.StreamWriter)
                {
                    result = viewTreeFactory.WriteJsonDoc(sessionFile.StreamWriter);

                }
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {

                    return;
                }

                if (LocalStateMachine.ViewState.HasFlag(ClassTreeViewState.ClassSelectRequest))
                {
                    SendTreeLevel(serviceAgentElastic.ClassList, Text_SelectedNodeId);
                }
                else if (LocalStateMachine.ViewState.HasFlag(ClassTreeViewState.ClassSearchRequest))
                {
                    PathNodes = new List<ViewTreeNode> { localConfig.SearchNode };
                    SendTreeLevel(serviceAgentElastic.ClassList, localConfig.SearchNode.Id);
                }
                else if (LocalStateMachine.ViewState.HasFlag(ClassTreeViewState.Initialize))
                {
                    Url_TreeData = sessionFile.FileUri.AbsoluteUri;
                }

                
                LocalStateMachine.ClassLoaded();
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultPathItems))
            {
                PathNodes = viewTreeFactory.CreatePathNodes(serviceAgentElastic.ResultPathItems.PathItems, Text_SelectedNodeId);
            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
