﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using ClassTreeController.Models;
using OntoMsg_Module.Models;
using OntoWebCore.Models;

namespace ClassTreeController.BusinessFactory
{
    public class ViewTreeFactory
    {

        private List<clsOntologyItem> classList;

        public List<ViewTreeNode> RootTreeNodes { get; private set; }
        public List<ViewTreeNode> TreeNodes { get; private set; }
        

        private clsLocalConfig localConfig;


        
        public ViewTreeFactory(clsLocalConfig localConfig)
        {
            
            this.localConfig = localConfig;

            
        }

        public ViewTreeNode CreateRootNode()
        {
            return new ViewTreeNode
            {
                Id = localConfig.Globals.Root.GUID,
                Name = localConfig.Globals.Root.Name
            };

        }

        public List<ViewTreeNode> CreatePathNodes(List<clsOntologyItem> classList, string idNode)
        {

            var nodeList = CreateTree(classList: classList);

            return nodeList;
        }

        public clsOntologyItem CreateTreeNodes(List<clsOntologyItem> classList, int level, string idParent = null)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            
            if (level != -1 && idParent != localConfig.SearchNode.Id)
            {
                var classItems = classList.Where(clsItem => clsItem.GUID_Parent == idParent).ToList();
                this.classList = new List<clsOntologyItem>();
                this.classList.AddRange(classItems);

                do
                {
                    this.classList.AddRange(from clsItm in classList
                                            join classItem in classItems on clsItm.GUID_Parent equals classItem.GUID
                                            select clsItm);
                    level--;
                } while (level >= 0);

                RootTreeNodes = new List<ViewTreeNode>();
                TreeNodes = new List<ViewTreeNode>();
                result = CreateTreeWithSubHtml(idParent: idParent);
            }
            else if (idParent != localConfig.SearchNode.Id)
            {
                this.classList = classList;
                RootTreeNodes = new List<ViewTreeNode>();
                TreeNodes = new List<ViewTreeNode>();
                result = CreateTreeWithSubHtml();
                this.classList = classList;
            }
            else
            {
                this.classList = new List<clsOntologyItem>();
                this.classList.AddRange(classList);

                RootTreeNodes = new List<ViewTreeNode>();
                RootTreeNodes.Add(localConfig.SearchNode);

                TreeNodes = new List<ViewTreeNode>();
                result = CreateTreeWithSubHtml(idParent: idParent);
            }

            

            return result;
        }

        public clsOntologyItem CreateTreeWithSubHtml(ViewTreeNode parentItem = null, string idParent = null)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var nodes = new List<ViewTreeNode>();

            if (parentItem == null && idParent == localConfig.SearchNode.Id)
            {
                parentItem = new ViewTreeNode
                {
                    Id = localConfig.SearchNode.Id,
                    Name = localConfig.SearchNode.Id
                };
            }
            if (parentItem != null)
            {
                if (parentItem.Id != localConfig.SearchNode.Id)
                {
                    nodes.Add(parentItem);
                }
                else
                {
                    nodes.AddRange(classList.Select(clsItem => new ViewTreeNode
                    {
                        Id = clsItem.GUID,
                        Name = clsItem.Name
                    }));
                }


                
                
            }
            else if (parentItem == null && idParent != null)
            {
               
                nodes = classList.Where(clsItm => clsItm.GUID_Parent == idParent).Select(clsItm => new ViewTreeNode
                {
                    Id = clsItm.GUID,
                    Name = clsItm.Name
                }).ToList();
               

                
            }
            else
            {
                nodes = classList.Where(clsItm => clsItm.GUID_Parent == localConfig.Globals.Root.GUID).Select(clsItm => new ViewTreeNode
                        {
                            Id = clsItm.GUID,
                            Name = clsItm.Name
                        }).ToList();
            }
            

            TreeNodes.AddRange(nodes);
            if (parentItem != null)
            {
                
                    parentItem.SubNodes = nodes;
                
                
            }
            else
            {
                RootTreeNodes.AddRange(nodes);
            }

            foreach (var node in nodes)
            {
                node.ChildCount = classList.Count(clsItm => clsItm.GUID_Parent == node.Id);
                if (node.ChildCount > 0)
                {
                    node.Html = string.Format(HTMLTemplates.ListDiv, System.Web.HttpUtility.HtmlEncode(node.Name), node.ChildCount.ToString(), "fa fa-plus-square-o");
                }
                else
                {
                    node.Html = string.Format(HTMLTemplates.ListDiv, System.Web.HttpUtility.HtmlEncode(node.Name), node.ChildCount.ToString(), "");
                }
                
            }

            return result;
        }
        public clsOntologyItem CreateTreeWithChildCount(ViewTreeNode parentItem = null)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var nodes = classList.Where(clsItm => parentItem == null ? clsItm.GUID_Parent == localConfig.Globals.Root.GUID : clsItm.GUID_Parent == parentItem.Id).Select(clsItm => new ViewTreeNode
            {
                Id = clsItm.GUID,
                Name = clsItm.Name
            }).ToList();

            TreeNodes.AddRange(nodes);
            if (parentItem != null)
            {
                parentItem.SubNodes = nodes;
            }
            else
            {
                RootTreeNodes.AddRange(nodes);
            }

            foreach (var node in nodes)
            {
                node.ChildCount = classList.Count(clsItm => parentItem == null ? clsItm.GUID_Parent == localConfig.Globals.Root.GUID : clsItm.GUID_Parent == parentItem.Id);
                
            }

            return result;
        }
        private List<ViewTreeNode> CreateTree(ViewTreeNode parentItem = null, List<clsOntologyItem> classList = null)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var nodeList = new List<ViewTreeNode>();
            var classListForNodes = classList != null ? classList : this.classList;

            if (parentItem == null)
            {
                parentItem = new ViewTreeNode
                {
                    Id = localConfig.Globals.Root.GUID,
                    Name = localConfig.Globals.Root.Name
                };
                nodeList.Add(parentItem);

                
            }
            var nodes = classListForNodes.Where(clsItm => clsItm.GUID_Parent == parentItem.Id).Select(clsItm => new ViewTreeNode
            {
                Id = clsItm.GUID,
                Name = clsItm.Name,
                ParentNode = parentItem
            }).ToList();

            nodeList.AddRange(nodes);
            
            //parentItem.SubNodes = nodes;
            
            foreach(var node in nodes)
            {
                var newNodes = CreateTree(node, classList);
                if (newNodes == null) return null;
                nodeList.AddRange(newNodes);
                
                
            }

            return nodeList;
        }

        public clsOntologyItem WriteJsonDoc(StreamWriter streamWriter)
        {
            try
            {
                bool success = true;
                using (var jsonWriter = new JsonTextWriter(streamWriter))
                {
                    jsonWriter.WriteStartArray();

                    var rootTreeNodes = RootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList();
                    var searchCheckNode = rootTreeNodes.FirstOrDefault(node => node.Id == localConfig.SearchNode.Id);
                    
                    if (searchCheckNode != null)
                    {
                        TreeNodes.OrderBy(nodeItem => nodeItem.Name).ToList().ForEach(nodeItem =>
                        {
                            nodeItem.WriteJSON(jsonWriter);
                        });
                    }
                    else
                    {
                        RootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList().ForEach(rootNode =>
                        {
                            rootNode.WriteJSON(jsonWriter);
                        });
                    }
                    
                    jsonWriter.WriteEndArray();
                }
                if (success)
                {
                    return localConfig.Globals.LState_Success.Clone();
                }
                else
                {
                    return localConfig.Globals.LState_Error.Clone();
                }

            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }
        }

        public ViewTreeNode CrateSearchNode()
        {
            return new ViewTreeNode
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Search"
            };
        }

        private void WriteJsonItem(JsonTextWriter jsonWriter, clsOntologyItem oItemParent)
        {
            jsonWriter.WriteStartObject();

            jsonWriter.WritePropertyName("id");
            jsonWriter.WriteValue(oItemParent.GUID);
            jsonWriter.WritePropertyName("label");
            jsonWriter.WriteValue(System.Web.HttpUtility.HtmlEncode(oItemParent.Name));

            var subClasses = classList.OrderBy(rootNode => rootNode.Name).Where(clsItem => clsItem.GUID_Parent == oItemParent.GUID).OrderBy(clsItem => clsItem.Name).ToList();
            if (subClasses.Any())
            {
                jsonWriter.WritePropertyName("items");
                jsonWriter.WriteStartArray();
                subClasses.OrderBy(subClass => subClass.Name).ToList().ForEach(clsItem =>
                {
                    WriteJsonItem(jsonWriter, clsItem);
                });
                jsonWriter.WriteEndArray();
            }
            
            jsonWriter.WriteEndObject();
        }
    }
}
