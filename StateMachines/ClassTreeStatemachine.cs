﻿using OntoMsg_Module.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassTreeController.StateMachines
{
    [Flags]
    public enum ClassTreeViewState
    {
        None = 0,
        Initialize = 1,
        ClassLoaded = 2,
        ClassSearchRequest = 4,
        ClassSelectRequest = 8
    }
    public class ClassTreeStatemachine : ControllerStateMachine
    {
        
        private ClassTreeViewState viewState;
        public ClassTreeViewState ViewState
        {
            get { return viewState; }
            private set
            {
                viewState = value;
                RaisePropertyChanged(nameof(ViewState));
            }
        }

        public bool ShowLoader
        {
            get
            {
                if (ViewState.HasFlag(ClassTreeViewState.Initialize))
                {
                    return true;
                }

                if (ViewState.HasFlag(ClassTreeViewState.ClassSearchRequest))
                {
                    return true;
                }

                if (ViewState.HasFlag(ClassTreeViewState.ClassSelectRequest))
                {
                    return true;
                }

                return false;
            }
        }

        public void Initialize()
        {
            ViewState = ClassTreeViewState.Initialize;
        }

        public void ClassLoaded()
        {
            ViewState = ClassTreeViewState.ClassLoaded;
        }

        public void ClassSearchRequest()
        {
            ViewState |= ClassTreeViewState.ClassSearchRequest;
        }

        public void ClassSelectRequest()
        {
            ViewState |= ClassTreeViewState.ClassSelectRequest;
        }

        public ClassTreeStatemachine(StateMachineType stateMachineType) : base(stateMachineType)
        {

        }
    }
}
